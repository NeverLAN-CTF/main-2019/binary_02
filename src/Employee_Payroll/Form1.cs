﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Employee_Payroll
{
    public partial class employee_payroll : Form
    {

        private int loginAttemptCount = 1;

        private int r1 = 0x66;
        private int r2 = 0x6c;
        private int r3 = 0x61;
        private int r4 = 0x67;
        private int r5 = 0x7b;
        private int r6 = 0x53;
        private int r7 = 0x54;
        private int r8 = 0x30;
        private int r9 = 0x52;
        private int r10 = 0x49;
        private int r11 = 0x4e;
        private int r12 = 0x47;
        private int r13 = 0x5f;
        private int r14 = 0x53;
        private int r15 = 0x54;
        private int r16 = 0x41;
        private int r17 = 0x54;
        private int r18 = 0x31;
        private int r19 = 0x43;
        private int r20 = 0x5f;
        private int r21 = 0x50;
        private int r22 = 0x41;
        private int r23 = 0x35;
        private int r24 = 0x35;
        private int r25 = 0x57;
        private int r26 = 0x4f;
        private int r27 = 0x52;
        private int r28 = 0x44;
        private int r29 = 0x53;
        private int r30 = 0x5f;
        private int r31 = 0x31;
        private int r32 = 0x4e;
        private int r33 = 0x5f;
        private int r34 = 0x46;
        private int r35 = 0x49;
        private int r36 = 0x4c;
        private int r37 = 0x33;
        private int r38 = 0x53;
        private int r39 = 0x5f;
        private int r40 = 0x31;
        private int r41 = 0x53;
        private int r42 = 0x5f;
        private int r43 = 0x4e;
        private int r44 = 0x30;
        private int r45 = 0x54;
        private int r46 = 0x5f;
        private int r47 = 0x53;
        private int r48 = 0x33;
        private int r49 = 0x43;
        private int r50 = 0x55;
        private int r51 = 0x52;
        private int r52 = 0x33;
        private int r53 = 0x7d;


        public employee_payroll()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {

            if (checkUsername() && checkPassword()) {

                StringBuilder sb = new StringBuilder();

                sb.Append(System.Convert.ToChar(r1).ToString());
                sb.Append(System.Convert.ToChar(r2).ToString());
                sb.Append(System.Convert.ToChar(r3).ToString());
                sb.Append(System.Convert.ToChar(r4).ToString());
                sb.Append(System.Convert.ToChar(r5).ToString());
                sb.Append(System.Convert.ToChar(r6).ToString());
                sb.Append(System.Convert.ToChar(r7).ToString());
                sb.Append(System.Convert.ToChar(r8).ToString());
                sb.Append(System.Convert.ToChar(r9).ToString());
                sb.Append(System.Convert.ToChar(r10).ToString());
                sb.Append(System.Convert.ToChar(r11).ToString());
                sb.Append(System.Convert.ToChar(r12).ToString());
                sb.Append(System.Convert.ToChar(r13).ToString());
                sb.Append(System.Convert.ToChar(r14).ToString());
                sb.Append(System.Convert.ToChar(r15).ToString());
                sb.Append(System.Convert.ToChar(r16).ToString());
                sb.Append(System.Convert.ToChar(r17).ToString());
                sb.Append(System.Convert.ToChar(r18).ToString());
                sb.Append(System.Convert.ToChar(r19).ToString());
                sb.Append(System.Convert.ToChar(r20).ToString());
                sb.Append(System.Convert.ToChar(r21).ToString());
                sb.Append(System.Convert.ToChar(r22).ToString());
                sb.Append(System.Convert.ToChar(r23).ToString());
                sb.Append(System.Convert.ToChar(r24).ToString());
                sb.Append(System.Convert.ToChar(r25).ToString());
                sb.Append(System.Convert.ToChar(r26).ToString());
                sb.Append(System.Convert.ToChar(r27).ToString());
                sb.Append(System.Convert.ToChar(r28).ToString());
                sb.Append(System.Convert.ToChar(r29).ToString());
                sb.Append(System.Convert.ToChar(r30).ToString());
                sb.Append(System.Convert.ToChar(r31).ToString());
                sb.Append(System.Convert.ToChar(r32).ToString());
                sb.Append(System.Convert.ToChar(r33).ToString());
                sb.Append(System.Convert.ToChar(r34).ToString());
                sb.Append(System.Convert.ToChar(r35).ToString());
                sb.Append(System.Convert.ToChar(r36).ToString());
                sb.Append(System.Convert.ToChar(r37).ToString());
                sb.Append(System.Convert.ToChar(r38).ToString());
                sb.Append(System.Convert.ToChar(r39).ToString());
                sb.Append(System.Convert.ToChar(r40).ToString());
                sb.Append(System.Convert.ToChar(r41).ToString());
                sb.Append(System.Convert.ToChar(r42).ToString());
                sb.Append(System.Convert.ToChar(r43).ToString());
                sb.Append(System.Convert.ToChar(r44).ToString());
                sb.Append(System.Convert.ToChar(r45).ToString());
                sb.Append(System.Convert.ToChar(r46).ToString());
                sb.Append(System.Convert.ToChar(r47).ToString());
                sb.Append(System.Convert.ToChar(r48).ToString());
                sb.Append(System.Convert.ToChar(r49).ToString());
                sb.Append(System.Convert.ToChar(r50).ToString());
                sb.Append(System.Convert.ToChar(r51).ToString());
                sb.Append(System.Convert.ToChar(r52).ToString());
                sb.Append(System.Convert.ToChar(r53).ToString());
                
                MessageBox.Show(sb.ToString());
                Application.Exit();
            }

            if (loginAttemptCount > 2)
            {
                showLoginCountExceeded();
                Application.Exit();
            }

            if (!checkUsername() || !checkPassword())
            {
                showError();
                loginAttemptCount++;
            }

        }

        private void showError()
        {
            MessageBox.Show("Username or Password is incorrect, please try again", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void showLoginCountExceeded()
        {
            MessageBox.Show("Too many login attempts", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private Boolean checkUsername()
        {
            if (txtUsername.Text == "admin")
            {
                return true;
            }
            return false;
        }

        private Boolean checkPassword()
        {
            if (txtPassword.Text == "dGhpc19pc19ub3RfdGhlX2ZsYWdfeW91X3NlZWtfa2VlcF9sb29raW5n")
            {
                return true;
            }
            return false;
        }
    }
}
