# Binary 2

#### Description
Our lead Software Engineer recently left and deleted all the source code and changed the login information for our employee payroll application. Without the login information none of our employees will be paid. Can you help us by finding the login information?

#### Flag
flag{ST0RING_STAT1C_PA55WORDS_1N_FIL3S_1S_N0T_S3CUR3}

#### Hints
How does this application validate the username/password?

#### Solution
* Use iSpy to decompile .NET exe
* Find username and password
* Log into application
* Display flag

Alternative
* Extract all hex values from variables r1 - r53
* Decode hex => ascii and combine for flag
